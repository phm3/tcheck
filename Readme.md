# T-check

A simple script that helps to execute command on the remote server, but by default it checks the path and prints out the files residing there. The script creates a file with "old entries" and on the second run, the new incoming data is compared against the old one. Nothing fancy. Built with `Python 2.7`

## Dependencies

* Paramiko `pip install paramiko`

## How it works

Edit the file and update the code at lines 24 and 25. 

`24. client.connect(remote, username="yourusername", password="anonymousareanonymous")`
`25. 	stdin, stdout, stderr = client.exec_command('your coommand')`

## Run

Simply pass remote hostname/IP as arg

`python tcheck.py 0.0.0.0 `

Well, that's it. 
